import QtQuick 2.5
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Component {
    id: component_AccountList

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        property alias edSearch: inner_edSearch

        Header {
            text: "Выбор аккаунта"
            isBack: true
            mouse.onClicked: {
                loader_Main.sourceComponent = component_MainMenu
            }
        }

        // Полоса для ввода и поиска
        RowLayout {
            spacing: 5
            Layout.fillWidth: true
            Layout.margins: 5

            Text {
                id: inner_labelSearch
                text: qsTr("Имя")
                font.pointSize: 12
                color: textColorNormal
            }
            
            TextField {
                id: inner_edSearch
                font.pointSize: 12
                Layout.fillWidth: true
                style: TextFieldStyle {
                    textColor: textColorNormal
                    background: Rectangle {
                        radius: 2
                        implicitWidth: 100
                        implicitHeight: 24
                        border.color: fieldBorder
                        border.width: 1
                        color: fieldBackground
                    }
                }
            }
            
            StButton {
                id: inner_btSearch
                text: "Поиск"
                mouse.onClicked: {
                    if (edSearch.text.length < 3)
                    {
                        dialog.title = "Ошибка"
                        dialog.text = "Строка поиска должа содержать больше 3 символов"
                        dialog.icon = StandardIcon.Warning
                        dialog.visible = true
                    }
                    else
                    {
                        accountListModel.update(inner_edSearch.text)
                    }
                }
            }
        }

        RowLayout {
            id: inner_view
//            width: parent.width
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle
            {
                border.color: "grey"
                border.width: 1
                anchors.fill: parent
                color: mainBackground
                ListView {
                    anchors.fill: parent
                    clip: true
                    model: accountListModel
                    delegate:  Rectangle {
                        width: parent.width
                        height: txName.height + 10
                        border.width: 1
                        border.color: delegateMauseArea.containsMouse ? fieldBorderHover : fieldBorder
                        color: delegateMauseArea.containsMouse ? fieldBackgroundHover : fieldBackground
                        
                        Text {
                            id: txName
                            text: nick_name
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            font.pointSize: 12
                            color: delegateMauseArea.containsMouse ? textColorHover : textColorNormal
                        }

                        MouseArea {
                            id: delegateMauseArea
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                console.log("index: ", index)
                                console.log("accountid: ", nick_name)
                                loader_Main.sourceComponent = component_TankList
                                accoutTankModel.setAccountID(account_id)
                                accoutTankModel.update()
                            }
                        }
                    }
                }
            }
        }
    }
}
