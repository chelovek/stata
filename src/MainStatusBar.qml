import QtQuick 2.5
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

StatusBar {
    style: StatusBarStyle {
        padding {
            left: 8
            right: 8
            top: 3
            bottom: 3
        }
        background: Rectangle {
            implicitHeight: 16
            implicitWidth: 200
            gradient: Gradient{
                GradientStop{
                    color: staticBackground
                    position: 0
                }
                GradientStop{
                    color: staticBackground2
                    position: 1
                }
            }
            Rectangle {
                anchors.top: parent.top
                width: parent.width
                height: 1
                color: "#999"
            }
        }
    }
    
    RowLayout {
        Layout.fillWidth: true
        Label {
            text: isTokenActived() ? config.nickname : "Не авторизован"
            color: textColorNormal
            font.pointSize: 12
        }
        Label {
            text: "(" + accountInfo.globalRating + ")"
            color: textColorNormal
            font.pointSize: 12
        }
        Label {
            text: "Gold: " + accountInfo.gold
            color: "gold"
            font.pointSize: 12
        }
        Label {
            text: "Credits: " + accountInfo.credits
            color: textColorNormal
            font.pointSize: 12
        }
    }
}
