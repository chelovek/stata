#include "config.h"

#include <QFile>
#include <QDebug>
#include "appinfo.h"

Config::Config(QObject *parent)
    : QObject(parent),
      m_Settings(QSettings::NativeFormat, QSettings::UserScope, ORGANIZATION_NAME, APPLICATION_NAME)
{
    qDebug() << "File config: " << m_Settings.fileName();

    m_Settings.sync();
    setToken(m_Settings.value(SETTINGS_TOKEN).toString());
    setAccountID(m_Settings.value(SETTINGS_ACCOUNT_ID).toInt());
    setNickname(m_Settings.value(SETTINGS_NICKNAME).toString());
}

Config::~Config()
{
    m_Settings.sync();
}

QString Config::token() const
{
    return m_token;
}

int Config::accountID() const
{
    return m_accountID;
}

QString Config::nickname() const
{
    return m_nickname;
}

void Config::setToken(QString token)
{
    if (m_token == token)
        return;

    m_token = token;
    emit tokenChanged(token);
}

void Config::setAccountID(int accountID)
{
    if (m_accountID == accountID)
        return;

    m_accountID = accountID;
    emit accountIDChanged(accountID);
}

void Config::setNickname(QString nickname)
{
    if (m_nickname == nickname)
        return;

    m_nickname = nickname;
    emit nicknameChanged(nickname);
}
