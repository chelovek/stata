import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Component {
    id: contactDelegate
//    signal activated(real xPosition, real yPosition)

    Rectangle {
        width: parent.width
        height: txName.height + 10
        border.width: 1
        border.color: "black"
        color: "gray"

        Text {
            id: txName
            text: "nick_name"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 12
            color: "gold"
        }

        MouseArea {
            id: ma
            anchors.fill: parent
            onClicked: {
                contactDelegate.activated(mouse.x, mouse.y)
            }
        }
    }
}
