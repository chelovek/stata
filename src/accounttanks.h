#ifndef ACCOUNTTANKS_H
#define ACCOUNTTANKS_H

#include <QObject>
#include <QAbstractListModel>
#include "net.h"
#include "encyclopediatanks.h"

// Demonsar1
//#define ACCOUNT_ID "16563835"

// Demonsar2
//#define ACCOUNT_ID "19174127"

// CheloveTank
#define ACCOUNT_ID "16683020"

class Tank
{
public:
    enum MarkOfMasteryType
    {
        None,
        Degree_3,
        Degree_2,
        Degree_1,
        Master
    };

    Tank(int tankID, MarkOfMasteryType markOfMastery, int wins, int battles, QString tankName,
         QUrl image_small)
    {
        m_tank_id = tankID;
        m_mark_of_mastery = markOfMastery;
        m_wins = wins;
        m_battles = battles;
        m_tank_name = tankName;
        m_image_small = image_small;
    }

    int TankID()
    {
        return m_tank_id;
    }

    MarkOfMasteryType MarkOfMastery()
    {
        return m_mark_of_mastery;
    }

    int Wins()
    {
        return m_wins;
    }

    int Buttles()
    {
        return m_battles;
    }

    QString TankeName()
    {
        return m_tank_name;
    }

    QUrl ImageSmall()
    {
        return m_image_small;
    }

private:
    int                 m_tank_id;            // ИД танка
    MarkOfMasteryType   m_mark_of_mastery;    // Уровень владения танком
    int                 m_wins;               // Побед
    int                 m_battles;            // Боев
    QString             m_tank_name;          // Имя танка
    QUrl                m_image_small;        // Маленькое изображение
};

enum TankRoles
{
    TankIDRole = Qt::UserRole + 1,
    NumberRole,
    MarkOfMasteryRole,
    WinsRole,
    BattlesRole,
    TankNameRole,
    PercentRole,
    LeftToFiftyRole,
    ImageSmall
};

class  AccountTanks : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(AccountTanksStatus status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(QString accountID READ accountID WRITE setAccountID NOTIFY accountIDChanged)
public:
    enum AccountTanksStatus
    {
        None,
        Updating,
        Done,
        Error
    };
    Q_ENUM(AccountTanksStatus)

    explicit AccountTanks(QObject *parent = 0);
    ~AccountTanks();
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;
    void AddTank(const Tank& tank);
    AccountTanksStatus status() const;
    int columnCount(const QModelIndex &parent) const;
    QString accountID() const;

signals:
    void statusChanged(AccountTanksStatus arg);
    void accountIDChanged(QString accountID);
    void ready();

public slots:
    void update();
    void Result(QJsonValue value);
    void setStatus(AccountTanksStatus arg);
    void setAccountID(QString accountID);

private:
    QList<Tank>             m_List;
    Net                     m_Net;
    AccountTanksStatus      m_status;
    QString                 m_accountID;
    EncyclopediaTanks       m_EncyclopediaTanks;
};



#endif // ACCOUNTTANKS_H
