#include <QVariant>
#include <QUrlQuery>
#include <QJsonArray>
#include <QDebug>
#include "accounttanks.h"

AccountTanks::AccountTanks(QObject *parent)
    : QAbstractTableModel(parent),
      m_Net("/wot/account/tanks/")
{
    connect(&m_Net, SIGNAL(signalDocReady(QJsonValue)), SLOT(Result(QJsonValue)));
    setStatus(AccountTanksStatus::None);
    m_EncyclopediaTanks.update();
}

AccountTanks::~AccountTanks()
{
}

int AccountTanks::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_List.count();
}

QVariant AccountTanks::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_List.size())
    {
        return QVariant();
    }

    Tank tank = m_List[index.row()];

    if (role == TankRoles::TankIDRole)
    {
        return tank.TankID();
    }

    if (role == TankRoles::MarkOfMasteryRole)
    {
        Tank::MarkOfMasteryType localMarkOfMastery = tank.MarkOfMastery();
        switch (localMarkOfMastery)
        {
            case Tank::Master:
                return "M";
            case Tank::Degree_1:
                return "1";
            case Tank::Degree_2:
                return "2";
            case Tank::Degree_3:
                return "3";
            default:
                return "";
        }
    }

    if (role == TankRoles::WinsRole)
    {
        return tank.Wins();
    }

    if (role == TankRoles::BattlesRole)
    {
        return tank.Buttles();
    }

    if (role == TankRoles::TankNameRole)
    {
        return tank.TankeName();
    }

    if (role == TankRoles::NumberRole)
    {
        return index.row() + 1;
    }

    if (role == TankRoles::PercentRole)
    {
        float percent = ((float)tank.Wins() / (float)tank.Buttles()) * 100.0;
        return QString::number(percent, 'f', 2);
    }

    if (role == TankRoles::LeftToFiftyRole)
    {
        int countBattles = (tank.Buttles() - (2 * tank.Wins()));
        return countBattles > 0 ? QString::number(countBattles) : "Готово";
    }

    if (role == TankRoles::ImageSmall)
    {
        return tank.ImageSmall().toString();
    }

    return QVariant();
}

QHash<int, QByteArray> AccountTanks::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NumberRole] = "number";
    roles[TankIDRole] = "tank_id";
    roles[MarkOfMasteryRole] = "mark_of_rmastery";
    roles[WinsRole] = "wins";
    roles[BattlesRole] = "battles";
    roles[TankNameRole] = "tank_name";
    roles[LeftToFiftyRole] = "left_to_fifty";
    roles[PercentRole] = "percent";
    roles[ImageSmall] = "image_small";
    return roles;
}

void AccountTanks::AddTank(const Tank &tank)
{
    beginInsertRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
    m_List << tank;
    endInsertRows();
}

AccountTanks::AccountTanksStatus AccountTanks::status() const
{
    return m_status;
}

void AccountTanks::update()
{
    beginRemoveRows(QModelIndex(), 0, rowCount(QModelIndex()));
    m_List.clear();
    endRemoveRows();

    qDebug() << "Update";
    if (m_accountID.isEmpty())
    {
        qWarning() << "Не задан ид аккаунта";
        setStatus(AccountTanks::Error);
        return ;
    }

    QUrlQuery query;
    query.addQueryItem("account_id", m_accountID);
    m_Net.Post(query);

    setStatus(AccountTanks::Updating);
}

void AccountTanks::Result(QJsonValue value)
{
    QJsonObject data = value.toObject();

    // Это список такнов опльзователя
    QJsonArray tankArray = data[m_accountID].toArray();

    QMap<int, ETank> list = m_EncyclopediaTanks.TankList();

    foreach (QJsonValue val, tankArray)
    {
        QJsonObject tankObject = val.toObject();
        int tank_id = tankObject["tank_id"].toInt();
        Tank::MarkOfMasteryType mark = (Tank::MarkOfMasteryType)tankObject["mark_of_mastery"].toInt();
        QJsonObject statistitics = tankObject["statistics"].toObject();
        int wins = statistitics["wins"].toInt();
        int battles = statistitics["battles"].toInt();

//        QString name = m_TankList[tank_id];
        ETank etank = list[tank_id];

        Tank tank(tank_id, mark, wins, battles, etank.ShortName(), etank.ImageSmall());
        AddTank(tank);
    }

    setStatus(AccountTanksStatus::Done);

    emit ready();
}

void AccountTanks::setStatus(AccountTanksStatus arg)
{
    if (m_status == arg)
        return;

    m_status = arg;
    emit statusChanged(arg);
}

void AccountTanks::setAccountID(QString accountID)
{
    if (m_accountID == accountID)
        return;

    m_accountID = accountID;
    emit accountIDChanged(accountID);
}

int AccountTanks::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return roleNames().size();
}

QString AccountTanks::accountID() const
{
    return m_accountID;
}
