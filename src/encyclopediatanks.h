#ifndef ENCYCLOPEDIATANKS_H
#define ENCYCLOPEDIATANKS_H

#include "net.h"

class ETank
{
public:
    ETank();
    ETank(QString short_name_i18n, QUrl image_small);
    QUrl ImageSmall() const;
    QString ShortName() const;

private:
    QString     m_ShortNamei18n;
    QUrl        m_ImageSmall;
};

class EncyclopediaTanks : public QObject
{
    Q_OBJECT
public:
    EncyclopediaTanks();
    void update();
    QMap<int, ETank> TankList();

signals:

public slots:
    void Result(QJsonValue value);

private:
    Net						m_Net;
    QMap<int, ETank>        m_TankList;
};

#endif // ENCYCLOPEDIATANKS_H
