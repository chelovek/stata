TEMPLATE = app

QT += qml quick widgets network
CONFIG += c++11

TARGET = stata

BIN_DIR = $$PWD/../bin
DESTDIR = $$BIN_DIR

DEFINES += "APP_NAME=\\\"$$TARGET\\\""

SOURCES += main.cpp \
    net.cpp \
    authlogin.cpp \
    accountlist.cpp \
    authloginview.cpp \
    accounttanks.cpp \
    encyclopediatanks.cpp \
    accountlistmodel.cpp \
    accountinfo.cpp \
    config.cpp \
    clansinfo.cpp

RESOURCES += qml.qrc
# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    net.h \
    authlogin.h \
    accountlist.h \
    appinfo.h \
    authloginview.h \
    accounttanks.h \
    encyclopediatanks.h \
    accountlistmodel.h \
    version.h \
    appversion.h \
    accountinfo.h \
    config.h \
    clansinfo.h

DISTFILES +=

