#ifndef AUTHLOGINVIEW_H
#define AUTHLOGINVIEW_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QVBoxLayout>
#include "authlogin.h"

class AuthLoginView : public QWidget
{
    Q_OBJECT
public:
    explicit AuthLoginView(QWidget *parent = 0);

    void ClearWidgets();
signals:

public slots:
    void slotStatusChanged(AuthLogin::Status status);

private:
    QPushButton         m_ButtonAuth;
    QLabel              m_lbInfo;
    QVBoxLayout         m_VLayout;
    AuthLogin           m_Login;
};

#endif // AUTHLOGINVIEW_H
