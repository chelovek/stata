import QtQuick 2.5
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

ApplicationWindow {
    // Основной цвет фона
    property color mainBackground: "#050507"

    // Цвета текста
    property color textColorNormal: "#eee"
    property color textColorHover: "gold"
    property color textColorInactive: "#bb8"

    // Цвета полей в таблицах
    property color fieldBackground: "#111"
    property color fieldBackgroundHover: "#110"
//    property color fieldBorder: "#332"
    property color fieldBorder: "#050505"
    property color fieldBorderHover: "#553"

    // Цвета для кнопок
    property color buttonBackground: "#111"
    property color buttonBackgroundHover: "#333"
    property color buttonBorder: "#332"
    property color buttonBorderHover: "gold"

    // Цвета для статичных объектов (например заголовок)
    property color staticBackground: "#141414"
    property color staticBackground2: "#303030"
    property color staticBorder: "#555"

    AppVersion {
        id: appVersion
    }

    id: root
    title: "Статистика - " + appVersion.version
    visible: true
    width: 600
    height: 600
    minimumWidth: 300
    minimumHeight: 300
    color: mainBackground
    statusBar: MainStatusBar {}

    //    Component_AuthLogin {
    //        id: component_AuthLogin
    //    }

    Component_AccountList {
        id: component_AccountList
    }

    Component_TankList {
        id: component_TankList
    }

    Component_Clan {
        id: component_Clan
    }

    Loader {
        id: loader_Main
        anchors.fill: parent
    }

    AccountListModel {
        id: accountListModel
        onReady: {

        }
    }

    AccountTanksModel {
        id: accoutTankModel
        onReady: {
            loader_Main.sourceComponent = component_TankList
        }
    }

    Component_MainMenu {
        id: component_MainMenu
    }

    MessageDialog {
        id: dialog
        onAccepted: {
            console.log(this.text)
        }
    }

    AuthLogin {
        id: authLogin
    }

    Config {
        id: config
    }

    AccountInfo {
        id: accountInfo
    }

    ClansInfo {
        id: clansInfo;
    }

    Component.onCompleted: {
        console.log("Complited")

        // Проверяем токен
        accountInfo.getInfo(config.token, config.accountID)
        loader_Main.sourceComponent = component_MainMenu
    }

    function isTokenActived()
    {
//        return (config.nickname !== "")
        return (config.token !== "")
    }
}
