import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Component {
    id: component_TankList

    ColumnLayout {
        anchors.fill: parent
        property Component backComponent: component_MainMenu

        Header {
            text: "Список танков"
            isBack: true
            mouse.onClicked: {
                loader_Main.sourceComponent = backComponent
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: mainBackground
            ScrollView {
                anchors.fill: parent
                style: ScrollViewStyle {
                    handle : Rectangle {
                        border.color: "lightgray"
                        border.width: 1
                        implicitWidth: 14
                        implicitHeight: 20
                        color: "black"
                    }

                    scrollBarBackground: Rectangle {
                        implicitHeight: 40
                        implicitWidth: 14
                        color: "grey"
                    }
                }
                ListView {
                    anchors.fill: parent
                    cacheBuffer: 4800
                    clip: true
                    model: accoutTankModel
                    delegate: AccountTanksDelegate {}
                    header: AccountTanksHeader {}
                }
            }
        }
    }
}
