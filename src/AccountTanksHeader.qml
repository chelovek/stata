import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Rectangle {
    width: parent.width
    height: 30
    color: fieldBackground
    border.color: fieldBorder
    border.width: 1

    RowLayout {
        Layout.margins: 5
        anchors.fill: parent

        LElement {
            id: numberField
            text: "#"
            width: 30
            alignment: Text.AlignHCenter
            colortext: textColorNormal
        }

        RowDelimiter {}

        LElement {
            id: tankNameField
            text: "Танк"
            implicitWidth: 100
            Layout.fillWidth: true
            alignment: Text.AlignLeft
            colortext: textColorNormal
        }

        RowDelimiter {}

        LElement {
            id: percentWinsField
            text: "%"
            width: 40
            alignment: Text.AlignHCenter
            colortext: textColorNormal
        }

        RowDelimiter {}

        LElement {
            id: battlesField
            text: "Боев"
            width: 45
            alignment: Text.AlignHCenter
            colortext: textColorNormal
        }

        RowDelimiter {}

        LElement {
            id: leftToFiftyField
            text: "До 50%"
            width: 65
            alignment: Text.AlignHCenter
            colortext: textColorNormal
        }
    }
}
