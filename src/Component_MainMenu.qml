import QtQuick 2.5
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Component {
    id: component_MainMenu

    ColumnLayout {
        anchors.fill: parent
        spacing: 5

        Header {
            text: "Главное меню"
        }

        MenuLine {
            text: "Авторизация"
            visible: !isTokenActived()
            mouse.onClicked: {
                authLogin.auth()
            }
        }

        MenuLine {
            text: "Проверить активацию"
            visible: !isTokenActived()
            mouse.onClicked: {
                authLogin.checkToken()

            }
        }

        MenuLine {
            text: "Статистика других игроков"
            mouse.onClicked: {
                loader_Main.sourceComponent = component_AccountList
            }
        }

        MenuLine {
            text: "Своя статистика"
            visible: isTokenActived()
            mouse.onClicked: {
                loader_Main.sourceComponent = component_TankList
                accoutTankModel.setAccountID(config.accountID)
                accoutTankModel.update()
                //                accoutTankModel.backComponent = component_MainMenu
            }
        }

        MenuLine {
            text: "Клан"
            visible: isTokenActived()
            mouse.onClicked: {
                loader_Main.sourceComponent = component_Clan
                clansInfo.update()
            }
        }

        // Заполнение остатка поля
        Rectangle {
            id: bottomFill
            implicitHeight: 5
            width: parent.width
            Layout.fillHeight: true
            color: mainBackground
        }
    }
}
