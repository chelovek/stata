import QtQuick 2.5
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Rectangle {
    property alias text: headerLabel.text
    property alias mouse: backButton.mouse
    property bool isBack: false

    Layout.fillWidth: true
    height: 40
    color: staticBackground
    border.width: 1
    border.color: staticBorder
    gradient: Gradient{
        GradientStop{color: staticBackground2 ; position: 1}
        GradientStop{color: staticBackground ; position: 0}
    }

    RowLayout {
        anchors.fill: parent

        StButton {
            id: backButton
            visible: isBack
            text: "<="
        }

        Rectangle {
            Layout.fillWidth: true
            implicitWidth: headerLabel.implicitWidth + 10
            height: parent.height
            color: "transparent"

            Text {
                id: headerLabel
                font.pointSize: 18
                color: textColorNormal
                anchors.centerIn: parent
            }
        }
    }
}
