import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Rectangle {
    width: parent.width
    height: 36
//    color: maElement.containsMouse ? fieldBackgroundHover : fieldBackground
    border.color: maElement.containsMouse ? fieldBorderHover : fieldBorder
    border.width: 1
    gradient: Gradient {
        GradientStop { position: 0.0; color: "#202020" }
        GradientStop { position: 0.1; color: "#101010" }
        GradientStop { position: 0.9; color: "#101010" }
        GradientStop { position: 1.0; color: "#202020" }
    }

    RowLayout {
        anchors.fill: parent

        LElement {
            text: number
            width: 30
            alignment: Text.AlignRight
            colortext: colorFromPercent(percentWins())
        }

        RowDelimiter {}

//        Image {
//            id: im
//            source: image_small
//        }


//        LElement {
//            implicitWidth: 100
//            Layout.fillWidth: true
//            text: tank_name
//            alignment: Text.AlignLeft
//            colortext: colorFromPercent(percentWins())
//        }

        Rectangle {
            implicitWidth: 100
            implicitHeight: 30
            color: "transparent"
//            Layout.leftMargin: 0
            Layout.fillHeight: true
            Layout.fillWidth: true

            Image {
                id: im
                source: image_small
                x: 0
                y: 3
            }

            Rectangle {
                color: "transparent"
                anchors.fill: parent

                Text {
                    id: textid
                    z: 10
                    x: 75
                    y: 0
                    width: parent.width - 75
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: 12
                    text: tank_name
                    horizontalAlignment: Text.AlignLeft
                    color: colorFromPercent(percentWins())
                }
            }
        }


        RowDelimiter {}

        LElement {
            id: percentField
            text: percentWins()
            width: 40
            alignment: Text.AlignHCenter
            colortext: colorFromPercent(percentWins())
        }

        RowDelimiter {}

        LElement {
            text: battles
            width: 45
            alignment: Text.AlignHCenter
            colortext: colorFromPercent(percentWins())
        }

        RowDelimiter {}

        LElement {
            text: left_to_fifty
            width: 65
            alignment: Text.AlignHCenter
            colortext: colorFromPercent(percentWins())
        }
    }

    MouseArea {
        id: maElement
        anchors.fill: parent
        hoverEnabled: true
    }

    function colorFromPercent(percent) {
        if (percent < 50)
        {
            return "red"
        }
        else if (percent < 60)
        {
            return "green"
        }
        else if (percent < 70)
        {
            return "blue"
        }
        else
        {
            return "purple"
        }
    }

    function percentWins() {
        var value = wins / battles * 100

        if (value === 100.00)
        {
            return value.toFixed(0)
        }

        return value.toFixed(2)
    }
}
