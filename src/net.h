#ifndef NET_H
#define NET_H

#include <QObject>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSslError>

#define API_HOST "https://api.worldoftanks.ru"
#define PREFIX "/wot"
#define APP_ID "c229c9c1ab65c80a7b2733967260b208"

class Net : public QObject
{
    Q_OBJECT
public:
    explicit Net(const QString urlPath, QObject *parent = 0);
    ~Net();
    void Post(QUrlQuery queryList);
    void Get(QUrl url);

signals:
    void signalDocReady(QJsonValue value);
    void signalDocError(QJsonObject error);

public slots:
    void slotReadyRead();
    void slotFinished();
    void slotError(QNetworkReply::NetworkError code);
    void slotSSLError(QList<QSslError> errorList);

private:
    QNetworkAccessManager   m_Nam;
    QNetworkReply*          m_Reply;
    QString                 m_UrlPath;
};

#endif // NET_H
