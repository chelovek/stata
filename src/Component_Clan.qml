import QtQuick 2.5
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Component {
    id: component_Clan

    ColumnLayout {
        anchors.fill: parent
        property Component backComponent: component_MainMenu

        Header {
            text: "Клан"
            isBack: true
            mouse.onClicked: {
                loader_Main.sourceComponent = backComponent
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: mainBackground
            ScrollView {
                anchors.fill: parent
                style: ScrollViewStyle {
                    handle : Rectangle {
                        border.color: "lightgray"
                        border.width: 1
                        implicitWidth: 14
                        implicitHeight: 20
                        color: "black"
                    }

                    scrollBarBackground: Rectangle {
                        implicitHeight: 40
                        implicitWidth: 14
                        color: "grey"
                    }
                }
                ListView {
                    anchors.fill: parent
                    cacheBuffer: 4800
                    clip: true
                    model: clansInfo
                    delegate: Rectangle {
                        width: parent.width
                        height: 30
//                        color: maElement.containsMouse ? fieldBackgroundHover : fieldBackground
                        border.color: maElement.containsMouse ? fieldBorderHover : fieldBorder
                        border.width: 1
                        gradient: Gradient {
                            GradientStop { position: 0.0; color: "#202020" }
                            GradientStop { position: 0.1; color: "#101010" }
                            GradientStop { position: 0.9; color: "#101010" }
                            GradientStop { position: 1.0; color: "#202020" }
                        }

                        LElement {
                            implicitWidth: 100
                            width: parent.width
                            Layout.fillWidth: true
                            text: name
                            alignment: Text.AlignHCenter
                            colortext: "green"
                        }

                        MouseArea {
                            id: maElement
                            anchors.fill: parent
                            hoverEnabled: true
                        }
                    }
                }
            }
        }
    }
}
