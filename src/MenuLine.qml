import QtQuick 2.5
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Rectangle {
    property alias text: txLabel.text
    property alias mouse: maItem

    Layout.fillWidth: true
    height: 38
    border.width: 1
    border.color: maItem.containsMouse ? "#222" : "#111"
//    color: maItem.containsMouse ? fieldBackgroundHover : fieldBackground
    gradient: Gradient {
        GradientStop { position: 0.0; color: "#202020" }
        GradientStop { position: 0.1; color: "#101010" }
        GradientStop { position: 0.9; color: "#101010" }
        GradientStop { position: 1.0; color: "#202020" }
    }

    Text {
        id: txLabel
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pointSize: 14
        color: maItem.containsMouse ? textColorHover : textColorNormal
    }
    
    MouseArea {
        id: maItem
        anchors.fill: parent
        hoverEnabled: true
    }
}
