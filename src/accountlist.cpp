#include <QUrlQuery>
#include <QJsonArray>
#include "accountlist.h"

AccountList::AccountList()
    : m_Net("/wot/account/list/")
{
    connect(&m_Net, SIGNAL(signalDocReady(QJsonValue)), SLOT(result(QJsonValue)));
}

int AccountList::Size() const
{
    return m_List.size();
}

Account AccountList::at(int index) const
{
    return m_List.at(index);
}

QList<Account> AccountList::List()
{
    return m_List;
}

QString AccountList::nickName(int index)
{
    return m_List.at(index).nickName();
}

void AccountList::update(QString search)
{
    QUrlQuery data;
    data.addQueryItem("search", search);
    data.addQueryItem("limit", "10");
    m_Net.Post(data);
}

void AccountList::result(QJsonValue value)
{
    QJsonArray arr = value.toArray();

    beginRemoveRows(QModelIndex(), 0, m_List.size() - 1);
    m_List.clear();
    endRemoveRows();

    foreach (QJsonValue val, arr)
    {
        QJsonObject obj = val.toObject();
        qint64 id = qRound64(obj["account_id"].toDouble());
        QString nick = obj["nickname"].toString();

        Account acc(id, nick);

        beginInsertRows(QModelIndex(), m_List.size(), m_List.size());
        m_List << acc;
        endInsertRows();

//        qDebug() << id << " " << nick;
    }    

    emit ready();
}

QModelIndex AccountList::index(int row, int column, const QModelIndex &parent) const
{
    return createIndex(row, column, parent.internalId());
}

QModelIndex AccountList::parent(const QModelIndex &child) const
{
    Q_UNUSED(child)
    return QModelIndex();
}

int AccountList::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_List.size();
}

int AccountList::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 1;
}

QVariant AccountList::data(const QModelIndex &index, int role) const
{
//    qDebug() << "Index: " << index << ", Role: " << role;

    if (!index.isValid())
    {
        return QVariant();
    }

    if (index.row() >= m_List.size() || index.column() > 1)
    {
        // Индекс выходит за пределы
        return QVariant();
    }

    if (role == AccountIDRole)
    {
        return QString::number(m_List.at(index.row()).ID());
    }

    if (role == NickNameRole)
    {
        return m_List.at(index.row()).nickName();
    }

    return QVariant();
}


QHash<int, QByteArray> AccountList::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles.insert(AccountIDRole, "account_id");
    roles.insert(NickNameRole, "nick_name");
    return roles;
}
