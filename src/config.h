#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>
#include <QSettings>

#define SETTINGS_TOKEN "token"
#define SETTINGS_ACCOUNT_ID "account_id"
#define SETTINGS_NICKNAME "nickname"

class Config : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString token READ token WRITE setToken NOTIFY tokenChanged)
    Q_PROPERTY(int accountID READ accountID WRITE setAccountID NOTIFY accountIDChanged)
    Q_PROPERTY(QString nickname READ nickname WRITE setNickname NOTIFY nicknameChanged)

public:
    explicit Config(QObject *parent = 0);
    ~Config();
    QString token() const;
    int accountID() const;
    QString nickname() const;

signals:
    void tokenChanged(QString token);
    void accountIDChanged(int accountID);
    void nicknameChanged(QString nickname);

public slots:
    void setToken(QString token);
    void setAccountID(int accountID);
    void setNickname(QString nickname);

private:
    QSettings       m_Settings;
    QString         m_token;
    int             m_accountID;
    QString         m_nickname;
};

#endif // CONFIG_H
