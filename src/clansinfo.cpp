#include "clansinfo.h"
#include <QUrlQuery>
#include <QJsonArray>

ClansInfo::ClansInfo(QObject *parent)
    : QAbstractTableModel(parent),
      m_Net("/wgn/clans/info/")
{
    connect(&m_Net, SIGNAL(signalDocReady(QJsonValue)), SLOT(result(QJsonValue)));

    m_clanId = 178489;
}

void ClansInfo::update()
{
    beginRemoveRows(QModelIndex(), 0, rowCount(QModelIndex()));
    m_MemberList.clear();
    endRemoveRows();

    QUrlQuery query;
    query.addQueryItem("clan_id", QString::number(m_clanId));
    query.addQueryItem("fields", "members,private");
    query.addQueryItem("extra", "private.online_members");
    query.addQueryItem("access_token", "7924bb719c2f74264fd04af5b42db1867fa2b537");
    m_Net.Post(query);
}

void ClansInfo::result(QJsonValue value)
{
    QJsonObject data = value.toObject();

    QJsonObject clan = data[QString::number(m_clanId)].toObject();
    QJsonArray members = clan["members"].toArray();

//    qDebug() << "Mebers: " << members;


//    foreach (QJsonValue val, members)
//    {
//        QJsonObject obj = val.toObject();
//        int accId = obj["account_id"].toInt();
//        QString name = obj["account_name"].toString();

//        Member member;
//        member.setMember(accId, name);
////        beginInsertRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
////        m_MemberList << member;
////        endInsertRows();
//    }

    QJsonObject privat = clan["private"].toObject();
    QJsonArray online = privat["online_members"].toArray();

    qDebug() << "Online: " << online;

    foreach (QJsonValue val, online)
    {
        int idOnline = val.toInt();
//        for (int i = 0; i < m_MemberList.size(); ++i)
//        {
//            if (m_MemberList[i].member_id() == id)
//            {
//                m_MemberList[i].setIsOnline(true);
//            }
//        }
        foreach (QJsonValue mem, members)
        {
            QJsonObject obj = mem.toObject();
            int accId = obj["account_id"].toInt();
            QString name = obj["account_name"].toString();
            if (idOnline == accId)
            {
                Member member;
                member.setMember(accId, name);
                member.setIsOnline(true);

                beginInsertRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
                m_MemberList << member;
                qDebug() << member.name();
                endInsertRows();
            }
        }
    }

    foreach (Member mem, m_MemberList)
    {
        if (mem.is_online())
        {
            qDebug() << mem.name();
        }
    }
}

void ClansInfo::setclanId(int clan_id)
{
    if (m_clanId == clan_id)
        return;

    m_clanId = clan_id;
    emit clanIdchanged(clan_id);
}

int ClansInfo::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_MemberList.size();
}

int ClansInfo::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 2;
}

QVariant ClansInfo::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_MemberList.size())
    {
        return QVariant();
    }

    Member member = m_MemberList[index.row()];

    if (role == NameRole)
    {
        return member.name();
    }

    if (role == IsOnlineRole)
    {
        return member.is_online();
    }

    return QVariant();
}

int ClansInfo::clanId() const
{
    return m_clanId;
}

QHash<int, QByteArray> ClansInfo::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[IsOnlineRole] = "isOnline";
    return roles;
}
