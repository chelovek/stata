#include <QUrlQuery>
#include <QDesktopServices>
#include <QUuid>
#include <QMessageBox>
#include <QFile>
#include <QDebug>
#include "authlogin.h"

// http://stata.16mb.com//?&status=ok&access_token=b378b6580d8f825d4e9c315a95deeeaa69f7e659&nickname=CheloveTank&account_id=16683020&expires_at=1437840371
// http://stata.16mb.com/?request_id={bf053750-eb7a-42e3-b5c0-f3f22ed14618}&status=ok&access_token=7b24f98565a9b4268a445cd23367961b6819b1db&nickname=CheloveTank&account_id=16683020&expires_at=1449699078

AuthLogin::AuthLogin(QObject *parent)
    : QObject(parent),
      m_Net("/wot/auth/login/")
{
    connect(&m_Net, SIGNAL(signalDocReady(QJsonValue)), SLOT(Result(QJsonValue)));
    connect(&m_Net, SIGNAL(signalDocError(QJsonObject)), SIGNAL(signalDocError(QJsonObject)));
}

AuthLogin::~AuthLogin()
{
}

QString AuthLogin::token() const
{
    return m_token;
}

QString AuthLogin::login() const
{
    return m_login;
}

QDateTime AuthLogin::exTime() const
{
    return m_exTime;
}

int AuthLogin::accountID() const
{
    return m_accountID;
}

QModelIndex AuthLogin::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(row);
    Q_UNUSED(column);
    Q_UNUSED(parent);

    return QModelIndex();
}

QModelIndex AuthLogin::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);

    return QModelIndex();
}

int AuthLogin::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return 0;
}

int AuthLogin::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return 0;
}

QVariant AuthLogin::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(index);

    switch (role)
    {
        case Token:
        {
            return token();
        }
        case AccountID:
        {
            return accountID();
        }
        case ExpiresAt:
        {
            return exTime();
        }
        case Login:
        {
            return login();
        }
        default:
            return QVariant();
    }
}

AuthLogin::Status AuthLogin::status() const
{
    return m_status;
}

void AuthLogin::auth()
{
    // Отправка запроса на создание токена
    m_RequestID = QUuid::createUuid().toString().remove("{").remove("}");

    QUrlQuery redirData;
    redirData.addQueryItem("request_id", m_RequestID);

    QUrl redirect(redirect_host);
    redirect.setQuery(redirData);

    qDebug() << "Redirect url: " << redirect.toString();

    QUrlQuery data;
    data.addQueryItem("redirect_uri", redirect.toString());
    data.addQueryItem("nofollow", "1");

    m_Net.Post(data);

    emit statusChanged(SendRequest);
}

void AuthLogin::Result(QJsonValue value)
{
    qDebug() << "Result value: " << value;

    QJsonObject obj = value.toObject();

    if (obj.contains("location"))
    {
        // После активации токена через WG
        QUrl authUrl = obj["location"].toString();

        if (!QDesktopServices::openUrl(authUrl))
        {
            QMessageBox msgBox;
            msgBox.setText("Не удалось открыть ссылку в браузере");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();

//            emit statusChanged(NoToken);
        }
    }
    else if (obj.contains("token"))
    {
        // Если токен еще активен
        setToken(obj["token"].toString());
        setLogin(obj["nickname"].toString());
        setExTime(QDateTime::fromString(obj["expires"].toString(), "yyyy-MM-dd hh:mm:ss"));
        setAccountID(obj["account_id"].toInt());

        qDebug() << "Get token: " << token();

        setStatus(Activated);
    }
    else {
//        emit statusChanged(Deactivated);
    }
}

void AuthLogin::setToken(QString token)
{
    if (m_token == token)
        return;

    m_token = token;
    setStatus(AuthLogin::Activated);
    emit tokenChanged(token);
}

void AuthLogin::UpdateStatus()
{
    emit statusChanged(Update);

    QUrl url(redirect_host);

    QUrlQuery query;
    query.addQueryItem("gettoken", "bbeba0aff686837c600fa6c9d7a77947b360aa79");

    url.setQuery(query);

    m_Net.Get(url);
}

void AuthLogin::setExTime(QDateTime exTime)
{
    if (m_exTime == exTime)
        return;

    m_exTime = exTime;
    emit exTimeChanged(exTime);
}

void AuthLogin::setAccountID(int accountID)
{
    if (m_accountID == accountID)
        return;

    m_accountID = accountID;
    emit accountIDChanged(accountID);
}

void AuthLogin::setStatus(AuthLogin::Status status)
{
    if (m_status == status)
        return;

    m_status = status;
    emit statusChanged(status);
}

void AuthLogin::checkToken()
{
    QUrl url(redirect_host);

    QUrlQuery query;
    query.addQueryItem("gettoken", m_RequestID);

    url.setQuery(query);

    m_Net.Get(url);
}

void AuthLogin::setLogin(QString login)
{
    if (m_login == login)
        return;

    m_login = login;
    emit loginChanged(login);
}
