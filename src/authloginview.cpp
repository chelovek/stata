#include <QDebug>
#include "authloginview.h"

AuthLoginView::AuthLoginView(QWidget *parent) : QWidget(parent)
{
    m_ButtonAuth.setText("Авторизация");

    m_VLayout.setContentsMargins(5, 5, 5, 5);
    m_VLayout.setSpacing(5);
    m_VLayout.addWidget(&m_lbInfo);
    m_VLayout.addWidget(&m_ButtonAuth);
    setLayout(&m_VLayout);

    connect(&m_Login, SIGNAL(statusChanged(AuthLogin::Status)), SLOT(slotStatusChanged(AuthLogin::Status)));
    connect(&m_ButtonAuth, SIGNAL(clicked(bool)), &m_Login, SLOT(auth()));

    m_Login.UpdateStatus();
}

void AuthLoginView::ClearWidgets()
{
    QLayoutItem *child;
    while ((child = m_VLayout.takeAt(0)) != 0)
    {
        delete child;
    }
}

void AuthLoginView::slotStatusChanged(AuthLogin::Status status)
{
    qDebug() << "Новое состояние: " << status;

    // Очистка виджетов из слоя
    ClearWidgets();

    switch (status)
    {
        case AuthLogin::Activated:
        {
            QString message = QString("Приложение активировано. Осталось %1 дней")
                    .arg(QDateTime::currentDateTimeUtc().daysTo(m_Login.exTime()));
            m_lbInfo.setText(message);

            m_VLayout.addWidget(&m_lbInfo);
            break;
        }
        case AuthLogin::Deactivated:
        {
            m_lbInfo.setText("Приложеие не активировано. Необходимо авторизоваться.");

            m_VLayout.addWidget(&m_lbInfo);
            m_VLayout.addWidget(&m_ButtonAuth);
            break;
        }
        case AuthLogin::Update:
        {
            m_lbInfo.setText("Получение информации об активации");

            m_VLayout.addWidget(&m_lbInfo);
            break;
        }
        case AuthLogin::Error:
        {
            m_lbInfo.setText("Возникла ошибка при получении текущего сочтояния токена\n"
                             "Попробуйте авторизоваться снова");

            m_VLayout.addWidget(&m_lbInfo);
            m_VLayout.addWidget(&m_ButtonAuth);
        }
        default:
            break;
    }
}


