#ifndef VERSION_H
#define VERSION_H

#define VERSION_STR "1.4.1"
#define CHANGE_LOG VERSION_STR \
" - Конфигурация перенесена в  QSettings::NativeScope (для винды это реестр)" \
\
"1.4.0" \
" - Добавлен список игноков из клана находящихся он лайн" \
\
"1.3.1" \
" - Версия программы теперь в одном месте version.h" \
\
"1.3.0" \

#endif // VERSION_H

