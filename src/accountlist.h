#ifndef ACCOUNTLIST_H
#define ACCOUNTLIST_H

#include <QObject>
#include <QAbstractItemModel>
#include "net.h"

class Account
{
public:
    Account(const qint64 id, const QString& nick)
    {
        m_ID = id;
        m_Nick = nick;
    }

    QString nickName() const
    {
        return m_Nick;
    }
    qint64 ID() const
    {
        return m_ID;
    }

private:
    QString m_Nick;
    qint64  m_ID;
};

class AccountList : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum AccountRoles
    {
        AccountIDRole = Qt::UserRole + 1,
        NickNameRole
    };

    AccountList();
    int Size() const;
    Account at(int index) const;
    QList<Account> List();
    QString nickName(int index);

signals:
    void ready();

public slots:
    void update(QString search);
    void result(QJsonValue value);

private:
    QList<Account>  m_List;
    Net             m_Net;
    bool            m_IsReady;

public:
    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;
};

#endif // ACCOUNTLIST_H
