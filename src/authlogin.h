#ifndef AUTHLOGIN_H
#define AUTHLOGIN_H

#include <QObject>
#include <QAbstractItemModel>
#include <QDateTime>
#include "appinfo.h"
#include "net.h"

class AuthLogin : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString token READ token WRITE setToken NOTIFY tokenChanged)
    Q_PROPERTY(QString login READ login WRITE setLogin NOTIFY loginChanged)
    Q_PROPERTY(QDateTime exTime READ exTime WRITE setExTime NOTIFY exTimeChanged)
    Q_PROPERTY(int accountID READ accountID WRITE setAccountID NOTIFY accountIDChanged)
    Q_PROPERTY(Status status READ status WRITE setStatus NOTIFY statusChanged)
    Q_ENUMS(Status)

public:
    enum Role
    {
        Token,
        Login,
        ExpiresAt,
        AccountID
    };

    enum Status
    {
        NoToken,            /// Нет токена
        SendRequest,        /// Отправлен запрос коллектору
        UnchecketToken,     /// Есть не проверенный токен
        CheckedToken,       /// Есть проверенный токен

        Activated,
        Deactivated,
        Update,
        Error
    };


    explicit AuthLogin(QObject *parent = 0);
    ~AuthLogin();
//    const QString link = "/auth/login/";
    const QString redirect_host = "http://stata.16mb.com/";

    QString token() const;
    QString login() const;
    QDateTime exTime() const;
    int accountID() const;
    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    Status status() const;

signals:
    void Ready();
    void tokenChanged(QString token);
    void loginChanged(QString login);
    void exTimeChanged(QDateTime exTime);
    void accountIDChanged(quint64 accountID);
    void statusChanged(AuthLogin::Status status);
    void signalDocError(QJsonObject error);

public slots:
    void auth();
    void Result(QJsonValue value);
    void setToken(QString token);
    void setLogin(QString login);
    void UpdateStatus();
    void setExTime(QDateTime exTime);
    void setAccountID(int accountID);
    void setStatus(Status status);
    void checkToken();

private:
    QString     m_token;
    QString     m_login;
    Net         m_Net;
    QDateTime   m_exTime;
    int         m_accountID;
    Status      m_status;
    QString     m_RequestID;
};

#endif // AUTHLOGIN_H
