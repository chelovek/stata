#ifndef CLANSINFO_H
#define CLANSINFO_H

#include <QObject>
#include <QAbstractListModel>
#include "net.h"

class Member
{
public:
    Member()
    {
        m_member_id = 0;
        m_is_online = false;
    }
    void setMember(int id, QString name)
    {
        m_member_id = id;
        m_name = name;
    }
    void setIsOnline(bool isOnline)
    {
        m_is_online = isOnline;
    }

    int member_id() const
    {
        return m_member_id;
    }

    QString name() const
    {
        return m_name;
    }

    bool is_online() const
    {
        return m_is_online;
    }

public slots:


private:
    int m_member_id;
    QString m_name;
    bool m_is_online;
};

enum MemberRoles
{
    IdRole = Qt::UserRole + 1,
    NameRole,
    IsOnlineRole
};

class ClansInfo : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(int clanId READ clanId WRITE setclanId NOTIFY clanIdchanged)
public:
    explicit ClansInfo(QObject *parent = 0);
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    int clanId() const;
    QHash<int, QByteArray> roleNames() const;

signals:
    void clanIdchanged(int clanId);

public slots:
    void update();
    void result(QJsonValue value);
    void setclanId(int clan_id);

private:
    QList<Member>       m_MemberList;
    Net                 m_Net;
    int                 m_clanId;
};

#endif // CLANSINFO_H
