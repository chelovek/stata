#include <QUrlQuery>
#include <QDebug>
#include "encyclopediatanks.h"

EncyclopediaTanks::EncyclopediaTanks()
    : m_Net("/wot/encyclopedia/tanks/")
{
    connect(&m_Net, SIGNAL(signalDocReady(QJsonValue)), SLOT(Result(QJsonValue)));
}

void EncyclopediaTanks::update()
{
    QUrlQuery data;
    data.addQueryItem("fields", "short_name_i18n,image_small");
//    data.addQueryItem("account_id", account_id);
    m_Net.Post(data);
}

QMap<int, ETank> EncyclopediaTanks::TankList()
{
    return m_TankList;
}

void EncyclopediaTanks::Result(QJsonValue value)
{
    QJsonObject data = value.toObject();

    // Это список всех танков
    foreach (QString key, data.keys())
    {
        QJsonValue value = data[key];
        QJsonObject obj = value.toObject();

        QString short_name = obj["short_name_i18n"].toString();
        QUrl url(obj["image_small"].toString());

        ETank tank(short_name, url);
        m_TankList[key.toInt()] = tank;
    }

    qDebug() << "==== Спиок танков получен ====";
}


ETank::ETank()
{

}

ETank::ETank(QString short_name_i18n, QUrl image_small)
{
    m_ShortNamei18n = short_name_i18n;
    m_ImageSmall = image_small;
}

QUrl ETank::ImageSmall() const
{
    return m_ImageSmall;
}

QString ETank::ShortName() const
{
    return m_ShortNamei18n;
}
