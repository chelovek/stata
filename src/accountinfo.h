#ifndef ACCOUNTINFO_H
#define ACCOUNTINFO_H

#include <QObject>
#include "net.h"

class AccountInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int accountID READ accountID WRITE setAccountID NOTIFY accountIDChanged)
    Q_PROPERTY(int clanID READ clanID WRITE setClanID NOTIFY clanIDChanged)
    Q_PROPERTY(int globalRating READ globalRating WRITE setGlobalRating NOTIFY globalRatingChanged)
    Q_PROPERTY(QString nickName READ nickName WRITE setNickName NOTIFY nickNameChanged)
    Q_PROPERTY(int gold READ gold WRITE setGold NOTIFY goldChanged)
    Q_PROPERTY(int credits READ credits WRITE setCredits NOTIFY creditsChanged)
    Q_PROPERTY(QList<int> frendList READ frendList WRITE setFrendList NOTIFY frendListChanged)

public:
    enum Role {
        AccountID,
        ClanID,
        GlobalRating,
        NickName
    };

    explicit AccountInfo(QObject *parent = 0);
    int accountID() const;
    int clanID() const;
    int globalRating() const;
    QString nickName() const;
    int gold() const;
    int credits() const;
    QList<int> frendList() const;


signals:
    void accountIDChanged(int accountID);
    void clanIDChanged(int clanID);
    void globalRatingChanged(int globalRating);
    void nickNameChanged(QString nickName);
    void goldChanged(int gold);
    void creditsChanged(int credits);
    void frendListChanged(QList<int> frendList);

public slots:
    void setAccountID(int accountID);
    void setClanID(int clanID);
    void setGlobalRating(int globalRating);
    void setNickName(QString nickName);
    void getInfo(QString tocken, QString accountID);
    void result(QJsonValue data);
    void setGold(int gold);
    void setCredits(int credits);
    void error(QString err);
    void setFrendList(QList<int> frendList);

private:
    int         m_accountID;
    int         m_clanID;
    int         m_globalRating;
    QString     m_nickName;
    Net         m_Net;
    int         m_gold;
    int         m_credits;
    QList<int> m_frendList;
};

#endif // ACCOUNTINFO_H
