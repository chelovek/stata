import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Rectangle {
    width: 3
    height: 30
    color: fieldBackground
    border.color: fieldBorder
    border.width: 1
}
