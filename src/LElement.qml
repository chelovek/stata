import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Rectangle {
    property alias text : textid.text
    property alias alignment : textid.horizontalAlignment
    property alias colortext: textid.color
    property int leftOffset: 0

    id: rootElemenet
    implicitHeight: 30
    color: "transparent"
    Layout.leftMargin: leftOffset
    Layout.fillHeight: true

    Text {
        id: textid
        z: 10
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        font.pointSize: 12
    }
}
