#ifndef APPVERSION_H
#define APPVERSION_H

#include <QString>
#include <QObject>
#include "version.h"

class AppVersion : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString version READ version CONSTANT)

public:
    QString version() const
    {
        return VERSION_STR;
    }

public slots:
signals:
private:
};

#endif // APPVERSION_H
