import QtQuick 2.5
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Rectangle {
    property alias text: txButton.text
    property alias mouse: maButton

    id: inner_btSearch
    anchors.verticalCenter: parent.verticalCenter
    implicitWidth: txButton.implicitWidth + 20
    implicitHeight: 25
    border.width: 1
    border.color: maButton.containsMouse ? buttonBorderHover : buttonBorder
    color: maButton.containsMouse ? buttonBackgroundHover : buttonBackground
    
    Text {
        id: txButton
        anchors.centerIn: parent
//        text: qsTr("Поиск")
        color: maButton.containsMouse ? textColorHover : textColorNormal
        font.pointSize: 12
    }
    
    MouseArea {
        id: maButton
        anchors.fill: parent
        hoverEnabled: true
    }
}
