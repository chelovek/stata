#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>
#include <QPair>
#include <QMessageBox>
#include <QDebug>

#include "net.h"

Net::Net(const QString urlPath, QObject *parent) : QObject(parent)
{
    m_Reply = NULL;
    m_UrlPath = urlPath;
}

Net::~Net()
{
}

void Net::Post(QUrlQuery queryList)
{
    QUrl url(API_HOST);
    url.setPath(m_UrlPath);

    queryList.addQueryItem("application_id", APP_ID);

    qDebug() << "URL: " << url.toString();
    QByteArray data;
    data.append(queryList.toString(QUrl::FullyEncoded).toUtf8());
    qDebug() << "Query: " << data;

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,
        "application/x-www-form-urlencoded");

    m_Reply = m_Nam.post(request, data);

    if (m_Reply != NULL)
    {
        connect(m_Reply, SIGNAL(readyRead()), SLOT(slotReadyRead()));
        connect(m_Reply, SIGNAL(finished()), SLOT(slotFinished()));
        connect(m_Reply, SIGNAL(error(QNetworkReply::NetworkError)),
                SLOT(slotError(QNetworkReply::NetworkError)));
        connect(m_Reply, SIGNAL(sslErrors(QList<QSslError>)), SLOT(slotSSLError(QList<QSslError>)));
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Ошибка при запросе авторизации на сервере Wargaming");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
}

void Net::Get(QUrl url)
{
    qDebug() << "URL: " << url.toString();
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,
        "application/x-www-form-urlencoded");
    m_Reply = m_Nam.get(request);
    if (m_Reply != NULL)
    {
        connect(m_Reply, SIGNAL(readyRead()), SLOT(slotReadyRead()));
        connect(m_Reply, SIGNAL(finished()), SLOT(slotFinished()));
        connect(m_Reply, SIGNAL(error(QNetworkReply::NetworkError)),
                SLOT(slotError(QNetworkReply::NetworkError)));
        connect(m_Reply, SIGNAL(sslErrors(QList<QSslError>)), SLOT(slotSSLError(QList<QSslError>)));
    }
}

void Net::slotReadyRead()
{
    qDebug() << "Ready read";

    QByteArray arr;
    arr = m_Reply->readAll();

//    qDebug() << arr;

    QJsonDocument doc;
    QJsonParseError error;
    doc = QJsonDocument(QJsonDocument::fromJson(arr, &error));

    QString status;
    QJsonObject object;
    if (error.error != QJsonParseError::NoError)
    {
        qDebug() << "Parse error: " << error.errorString();
        status = "error";

        QMessageBox msgBox;
        msgBox.setText(tr("Ошибка при обработке ответа от сервера Wargaming"));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
    else
    {
        object = doc.object();

//        qDebug() << object;

        status = object["status"].toString();
    }

    qDebug() << "Status: " << status;
    if (status == "ok")
    {
//        qDebug() << "Meta: " << object["meta"];
//        qDebug() << "Data: " << object["data"];

        emit signalDocReady(object["data"]);
    }
    else
    {
        qDebug() << "Error: " << object["error"];

        QJsonObject error = object["error"].toObject();
        QString msg = QString("Code: %1\n").arg(error["code"].toDouble());
        msg += QString("Message: %1\n").arg(error["message"].toString());
        msg += QString("Field: %1\n").arg(error["field"].toString());
        msg += QString("Value: %1").arg(error["value"].toString());        
        QMessageBox msgBox;
        msgBox.setText(msg);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();

        emit signalDocError(error);
    }

    qDebug() << "Data readed";
}

void Net::slotFinished()
{
    qDebug() << "Finished";
}

void Net::slotError(QNetworkReply::NetworkError code)
{
    qDebug() << "Error: [" << code << "] " << m_Reply->errorString();
    QMessageBox msgBox;
    msgBox.setText("Error:  " + m_Reply->errorString());
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.exec();
}

void Net::slotSSLError(QList<QSslError> errorList)
{
    qDebug() << errorList;
    QString errolMsg;
//    foreach (QSslError se, errorList)
//    {
//        errolMsg += " - " + se.errorString() + "\n";
//    }
    QMessageBox msgBox;
    msgBox.setText("Error:  \n" + errolMsg);
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.exec();
}
