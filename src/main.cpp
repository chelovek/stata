#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QLabel>
#include <QPicture>
#include <QDebug>
#include <QFile>
#include "version.h"
#include "appversion.h"
#include "authlogin.h"
#include "accountlist.h"
#include "accounttanks.h"
#include "accountinfo.h"
#include "config.h"
#include "clansinfo.h"
//#include "accountlistmodel.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationVersion(VERSION_STR);

    qDebug() << "App name: " << APP_NAME << " " << VERSION_STR;

    qmlRegisterType<AppVersion>("com.stata", 1, 0, "AppVersion");
    qmlRegisterType<AuthLogin>("com.stata", 1, 0, "AuthLogin");
//    qmlRegisterType<AccountList>("com.teamlist", 1, 0, "AccountList");
    qmlRegisterType<AccountTanks>("com.stata", 1, 0, "AccountTanksModel");
    qmlRegisterType<AccountList>("com.stata", 1, 0, "AccountListModel");
    qmlRegisterType<Config>("com.stata", 1, 0, "Config");
    qmlRegisterType<AccountInfo>("com.stata", 1, 0, "AccountInfo");
    qmlRegisterType<ClansInfo>("com.stata", 1, 0, "ClansInfo");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
