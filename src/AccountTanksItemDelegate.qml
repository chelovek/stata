import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import com.stata 1.0

Item {
    height: 30
    width: 30
    Text {
        anchors.verticalCenter: parent.verticalCenter
        color: colorPercent()
        elide: styleData.elideMode
        text: styleData.value        
    }

    function colorPercent() {

        if (styleData.column === 2)
        {
            if (styleData.value < 50)
            {
                return "red"
            }
            else
            {
                return "green"
            }
        }
        else
        {
            return styleData.textColor
        }
    }
}
