#include "accountinfo.h"

#include <QUrlQuery>

AccountInfo::AccountInfo(QObject *parent)
    : QObject(parent),
      m_globalRating(0),
      m_Net("/wot/account/info/"),
      m_gold(0),
      m_credits(0)
{
    connect(&m_Net, SIGNAL(signalDocReady(QJsonValue)), SLOT(result(QJsonValue)));
    connect(&m_Net, SIGNAL(signalDocError(QJsonObject)), SLOT(error(QJsonObject)));
}

int AccountInfo::accountID() const
{
    return m_accountID;
}

int AccountInfo::clanID() const
{
    return m_clanID;
}

int AccountInfo::globalRating() const
{
    return m_globalRating;
}

QString AccountInfo::nickName() const
{
    return m_nickName;
}

int AccountInfo::gold() const
{
    return m_gold;
}

int AccountInfo::credits() const
{
    return m_credits;
}

QList<int> AccountInfo::frendList() const
{
    return m_frendList;
}

void AccountInfo::setAccountID(int accountID)
{
    if (m_accountID == accountID)
        return;

    m_accountID = accountID;
    emit accountIDChanged(accountID);
}

void AccountInfo::setClanID(int clanID)
{
    if (m_clanID == clanID)
        return;

    m_clanID = clanID;
    emit clanIDChanged(clanID);
}

void AccountInfo::setGlobalRating(int globalRating)
{
    if (m_globalRating == globalRating)
        return;

    m_globalRating = globalRating;
    emit globalRatingChanged(globalRating);
}

void AccountInfo::setNickName(QString nickName)
{
    if (m_nickName == nickName)
        return;

    m_nickName = nickName;
    emit nickNameChanged(nickName);
}

void AccountInfo::getInfo(QString token, QString accountID)
{
    setAccountID(accountID.toInt());

    QStringList necessaryFields;
    necessaryFields << "global_rating";
    necessaryFields << "clan_id";
    necessaryFields << "nickname";
    necessaryFields << "private.gold";
    necessaryFields << "private.credits";

//    QStringList extraFields;
//    extraFields << "private.grouped_contacts";

    QUrlQuery data;
    data.addQueryItem("access_token", token);
    data.addQueryItem("account_id", accountID);
    data.addQueryItem("fields", necessaryFields.join(","));
//    data.addQueryItem("extra", extraFields.join(","));

    m_Net.Post(data);
}

void AccountInfo::result(QJsonValue data)
{
    qDebug() << "Result: " << data;

    QJsonObject obj = data.toObject();

    QString accountidStr = QString::number(accountID());

    if (obj.contains(accountidStr))
    {
        QJsonObject acc = obj[accountidStr].toObject();

        setGlobalRating(acc["global_rating"].toInt());
        setClanID(acc["clan_id"].toInt());
        setNickName(acc["nickname"].toString());

        if (acc.contains("private"))
        {
            QJsonObject priv = acc["private"].toObject();
            setGold(priv["gold"].toInt());
            setCredits(priv["credits"].toInt());

//            qDebug() << priv["grouped_contacts"];
        }
    }
}

void AccountInfo::setGold(int gold)
{
    if (m_gold == gold)
        return;

    m_gold = gold;
    emit goldChanged(gold);
}

void AccountInfo::setCredits(int credits)
{
    if (m_credits == credits)
        return;

    m_credits = credits;
    emit creditsChanged(credits);
}

void AccountInfo::error(QString err)
{

}

void AccountInfo::setFrendList(QList<int> frendList)
{
    if (m_frendList == frendList)
        return;

    m_frendList = frendList;
    emit frendListChanged(frendList);
}
